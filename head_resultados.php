﻿<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-76419537-6"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-76419537-6');
</script>

    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<meta name="keywords" content="Iluminación led, iluminación hogar, iluminación profesional, iluminación industrial, accesorios y componentes iluminación, iluminacion, led, companas, campanas led, tubos, tubos led, embutidos, lámparas, Opple Chile, opple, iluminacion Chile, insumos iluminación led, ampolletas, ampolletas led, iluminacion oficina, distribucion led, cintas led."/>
<meta name="rights" content="OPPLE CHILE, ILUMINACIÓN LED" />
<meta name="description" content="Comercialización de iluminación Led, iluminación led industrial, iluminación led profesional, iluminación led hogar, iluminación Led, expertos en iluminación, representante oficial de OPPLE." />
<title>OPPLE CHILE, ILUMINACIÓN LED</title>
<!-- Smartsupp Live Chat script -->
<script type="text/javascript">
var _smartsupp = _smartsupp || {};
_smartsupp.key = 'b77f9f367e12007a71c4b23dd20ff3fd6a29c9ba';
window.smartsupp||(function(d) {
  var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
  s=d.getElementsByTagName('script')[0];c=d.createElement('script');
  c.type='text/javascript';c.charset='utf-8';c.async=true;
  c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
})(document);
</script>
	

    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <link href="css/contenedorinternasbusqueda.css" rel="stylesheet" type="text/css" />

    <script src="fb/jquery.bpopup.min.js"></script>

    <style type="text/css">

        <!--

        body {

            margin-left: 0px;

            margin-top: 0px;

            margin-right: 0px;

            margin-bottom: 0px;

        }

        .style3 {color: #1952AE; font-size: 18px;}

        -->

    </style>

    <script type="text/JavaScript">

        <!--

        function MM_swapImgRestore() { //v3.0

            var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;

        }



        function MM_preloadImages() { //v3.0

            var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();

                var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)

                    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}

        }



        function MM_findObj(n, d) { //v4.01

            var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {

                d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}

            if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];

            for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);

            if(!x && d.getElementById) x=d.getElementById(n); return x;

        }



        function MM_swapImage() { //v3.0

            var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)

                if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}

        }

        function cotizacion() {

            $('#popup').bPopup({

                loadUrl: 'detalle_cotizacion.php', //Uses jQuery.load()

                closeClass:'close1',

                onClose: function() {

                    location.reload();

                }

            });

        }

        //-->

    </script>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '419200395266112');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=419200395266112&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>