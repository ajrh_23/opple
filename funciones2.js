$(document).ready(function () {

    var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;

    var num = /^[0-9]+$/;

    $("#boton").click(function (){

        $(".error").remove();
        if( $("#rut").val() == "" ){
            $("#rut").focus().after("<span class='error2'>Por favor, ingrese su Rut</span>");
            return false;
        }else if(document.getElementById('region').selectedIndex == 0){
            $("#region").focus().after("<span class='error2'>Por favor, ingrese su regi\u00f3n</span>");
            return false;
        }else if(document.getElementById('comuna').selectedIndex == 0){
            $("#comuna").focus().after("<span class='error2'>Por favor, ingrese su comuna</span>");
            return false;
        }else if( $("#direccion").val() == "" ){
            $("#direccion").focus().after("<span class='error2'>Por favor, ingrese su direcci\u00f3n</span>");
            return false;
        }else if( $("#telefono").val() == "" || !num.test($("#telefono").val()) ) {
            $("#telefono").focus().after("<span class='error2'>Por favor, ingrese su tel\u00e9fono (solo n\u00fameros)</span>");
            return false;
        }else if( $("#nombre").val() == "" ){
            $("#nombre").focus().after("<span class='error2'>Por favor, ingrese su nombre</span>");
            return false;
        }else if( $("#email").val() == "" || !emailreg.test($("#email").val()) ){
            $("#email").focus().after("<span class='error2'>Por favor, ingrese un correo v\u00e1lido</span>");
            return false;
        }
    });
    $("#rut, #direccion, #telefono, #nombre, #email").keyup(function(){
        if( $(this).val() != "" ){
            $(".error2").fadeOut();
            return false;

        }

    });
    $("#email").keyup(function(){
        if( $(this).val() != "" && emailreg.test($(this).val())){
            $(".error2").fadeOut();
            return false;
        }
    });

    $("#telefono").keyup(function(){
        if( $(this).val() != "" && num.test($(this).val())){
            $(".error2").fadeOut();
            return false;
        }
    });

    $("#telefono").keyup(function(){
        if( $(this).val() != "" ){
            $(".error2").fadeOut();
            return false;
        }
    });

});