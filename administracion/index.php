<?php
session_start();
session_destroy();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Opple Chile</title>
		<!-- CSS -->
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<link rel="stylesheet" href="css/social-icons.css" type="text/css" media="screen" />
		<!--[if IE 8]>
			<link rel="stylesheet" type="text/css" media="screen" href="css/ie8-hacks.css" />
		<![endif]-->
	</head>
    <script type="text/javascript">
	function validaform()
	{
		if(document.acceso.user.value!='' && document.acceso.pass.value!=''){
			document.acceso.submit();
		}else{
			if(document.acceso.user.value==''){
				alert("Ingrese su usuario para ingresar al Sistema");
				document.acceso.user.focus();
			}else if(document.acceso.pass.value==''){
				alert("Ingrese su contraseña de Seguridad para ingresar al Sistema");
				document.acceso.pass.focus();
			}
		}
	}
	</script>
	<style>
    .centro{
	text-align:center;
	height: 125px;
	padding-top: 12px;
    }
    .style3 {
        font-family: "Trebuchet MS";
        font-weight: bold;
        font-size: 24px;
    }
    .style4 {
        font-family: "Trebuchet MS";
        font-size: 13px;
    }
    .style5 {font-size: 13px}
    </style>	
	<body>
    <!-- HEADER -->
    <div id="header">
	<!-- wrapper-header -->
    	<div class="centro"><img src="images/logo.png" width="220" height="94"></div>
	<!-- ENDS wrapper-header -->					
	</div>
	<!-- ENDS HEADER -->
    <div id="main">
    	<!-- wrapper-main -->
        <div class="wrapper">
        	<!-- content -->
            <div id="content">
            <!-- title -->
            	<div id="page-title">
                	<p>&nbsp;</p>
					<h1 align="center" class="style3">Acceso Panel Administraci&oacute;n</h1>
		        </div>
                <!-- ENDS title -->
                <!-- column (left)-->
				<div class="centro">
                <form name="acceso" action="control.php" method="post">
                <table width="282" align="center">
                  <tr>
                    <td height="32" align="left" valign="top" class="textoadministrador style2">&nbsp;</td>
                    <td height="32" colspan="2" align="left" valign="top" class="textoadministrador style2 style5">Bienvenido al Panel de Administraci&oacute;n</td>
                  </tr>
                  <tr>
                    <td width="22" align="left" valign="middle" class="textoadministrador22">&nbsp;</td>
                    <td width="60" align="left" valign="middle" class="textoadministrador22">Usuario</td>
                    <td width="212"><input name="user" type="text" class="textare-estilo12" size="27" maxlength="18" /></td>
                  </tr>
                  <tr>
                    <td align="left" valign="middle" class="textoadministrador22">&nbsp;</td>
                    <td align="left" valign="middle" class="textoadministrador22">Contrase&ntilde;a</td>
                    <td><input name="pass" type="password" class="textare-estilo12" size="27" maxlength="18" /></td>
                  </tr>
                  <tr>
                    <td colspan="3" align="center" valign="bottom">
                      <div align="center">
                      <table width="240" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="234" height="25" align="right" valign="bottom"><input name="ir" type="button" class="botontextarea3" value="Ingresar" onClick="validaform()" /></td>
                        </tr>
                      </table>
                      </div>
                    </td>
                  </tr>
                </table>
                <br>
                </form>    
                <?php
                if($_GET["err"]=="1"){ 
                ?>
                <table width="257" align="center">
                  <tr>
                    <td class="remove-box style4">Usuario y contrase&ntilde;a incorrecto</td>
                  </tr>
                </table>
                <?php
                }?>
                </div>
				<!-- ENDS column -->
			</div>
			<!-- ENDS content -->
		</div>
		<!-- ENDS wrapper-main -->
	</div>
	<!-- ENDS MAIN -->
	</body>
</html>