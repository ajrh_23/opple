<?php 
include("head.php");
$busca_categorias = mysql_query("select * from categorias where subcategoria = 1") or die(mysql_error());
?>
<script type="text/javascript">
function validaForm(){
    var nombreCategoria = document.crear.nombre.value.length;
    var categoriaPrincipal = document.crear.categoria.selectedIndex;
    var imagenCategoria = document.crear.logo.value.length;
    
    if(nombreCategoria != 0 && imagenCategoria != 0 && categoriaPrincipal !=0){
        document.crear.submit();
    }else{
        if(nombreCategoria == 0){
            alert("El nombre de su subcategoria es obligatorio");
            document.crear.nombre.focus();
        }else if(categoriaPrincipal == 0){
            alert("La relacion con una categoria principal es obligatorio");
        }else{
            alert("El logo de su subcategoria es obligatorio");
        }
    }   
}
function controlLogo(f){
	var ext=['jpg','png','gif'];
	var v=f.value.split('.').pop().toLowerCase();
	for(var i=0,n;n=ext[i];i++){
		if(n.toLowerCase()==v)
			return
	}
	var t=f.cloneNode(true);
	t.value='';
	f.parentNode.replaceChild(t,f);
	alert('Extensi\u00f3n no v\u00e1lida, solo archivos PNG, JPG o GIF');
}
</script>
<div id="main">
	<div class="wrapper">
    	<div id="content">
        	<div id="page-title">
            	<span class="title">Crear Subcategor&iacute;as</span>
                <span class="subtitle">CENTRO MAQUINAS MINERAS</span>
			</div>
            <h4 align="center">&nbsp;</h4>
            <form name="crear" action="subcategorias_ctrl.php" method="post" enctype="multipart/form-data">
            <table width="578" align="center">
              <tr>
              	<td align="center" colspan="2"><span class="textoadministrador">Nombre de la subcategor&iacute;a </span><br><input name="nombre" size="50" maxlength="255" style="font-family:Verdana, Geneva, sans-serif" type="text" class="textare-estilo" value="" /></td>
              </tr>
              <tr>
              	<td colspan="2"><p></p></td>
              </tr>
              <tr>
              	<td align="center" colspan="2"><span class="textoadministrador">Relaci&oacute;n categor&iacute;a principal </span><br>
                  <select name="categoria" style="font-family:Verdana, Geneva, sans-serif">
                  <option value="">Seleccione</option>
                  <?php 
                  while($categoria = mysql_fetch_assoc($busca_categorias)){
                    ?>
                    <option value="<?php echo $categoria['id_categoria']?>"><?php echo $categoria['nombre']?></option>
                    <?php
                  }
                  ?>
                  </select>
                  </td>
              </tr>
              <tr>
              	<td colspan="2"><p></p></td>
              </tr>
              <tr>
                <td colspan="2" align='center'><span class="textoadministrador">Logo de su subcategor&iacute;a</span><br /><input name="logo" type="file" class="textare-estilo" onchange="controlLogo(this)" /></td>
              </tr>
              <tr>
              	<td colspan="2"><p></p></td>
              </tr>
              <tr>
              	<td colspan="2" align="center"><input name="continuar" type="button" class="botontextarea" value="Crear" onclick="validaForm()"></td>
              </tr>
            </table>
            </form>
		</div>
	</div>
</div>
<?php
include("footer.php");
?>
<?php
if($_GET['err']=="1"){
    $msg=utf8_encode("Error al cargar subcategoria, ya existe una con este nombre");
    echo '<script>alert("'.$msg.'");</script>';
}else if($_GET['err']=="logo"){
    switch($_GET['tip']){
        case "0":$msg=utf8_encode("Error al cargar logo, contactarse con el administrador");break;
        case "weight":$msg=utf8_encode("Error al cargar logo, archivo supera el peso m�ximo permitido");break;
    }
    echo '<script>alert("'.$msg.'");</script>';
}else if($_GET['err']=="ok"){
    $msg = utf8_encode("Subcategor�a creada correctamente");
    echo '<script>alert("'.$msg.'");</script>';
    echo '<script>document.location="b&g_subcategoria.php"</script>';
}
?>