<?php
include("head.php");
$botonesSql = mysql_query("SELECT * FROM `menu` where estado = 1 order by id asc", $dbh) or die(mysql_error());
$b_paginas = mysql_query("select * from paginas order by nombre", $dbh) or die(mysql_error());
$b_submenu = mysql_query("SELECT * FROM `submenu` order by id asc", $dbh) or die(mysql_error());
?>
    <div id="main">
        <div class="wrapper">
            <div id="content">
                <div id="page-title">
                    <span class="title">Gesti&oacute;n de Botones</span>
                    <span class="subtitle">Opple Chile</span>
                </div>
                <form action="subboton_ctrl.php?opc=1" method="post" onsubmit="return validaForm()">
                    <table width="845" border="0" align="center" bordercolor="#FFFFFF">
                        <tr>
                            <td>&nbsp;</td>
                            <td height="30" valign="top"><i class="textoadministrador"><strong>Sub
                                        Men&uacute</strong></i></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center"><i class="textoadministrador">Agregar</i></td>
                        </tr>
                        <tr>
                            <td colspan="2">Nombre: <input name="nombre" id="nombre" type="text" value=""
                                                           class="textare-estilo"/></td>
                            <td colspan="2">Men&uacute; Principal:
                                <select name="menu_principal" id="menu_principal" class="textare-estilo2">
                                    <option value="">Seleccione</option>
                                    <?php
                                    while ($row = mysql_fetch_assoc($botonesSql)) {
                                        ?>
                                        <option value="<?php echo $row['id'] ?>"><?php echo utf8_encode($row['boton']) ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                            <td colspan="2">Página:
                                <select name="pagina" id="pagina" class="textare-estilo2">
                                    <option value="#"<? if ($boton["link"] == "" || $boton['link'] == "#") {
                                        echo "selected";
                                    } ?>>Sin P&aacute;gina
                                    </option>
                                    <option value="index.php" <? if ($boton["link"] == "index.php") {
                                        echo "selected";
                                    } ?>>Inicio
                                    </option>
                                    <option value="productos.php" <? if ($boton["link"] == "productos.php") {
                                        echo "selected";
                                    } ?>>Productos
                                    </option>
                                    <option value="contacto.php" <? if ($boton["link"] == "contacto.php") {
                                        echo "selected";
                                    } ?>>Contacto
                                    </option>
                                    <option value="simulador.php" <? if ($boton["link"] == "simulador.php") {
                                        echo "selected";
                                    } ?>>Simulador de Consumo
                                    </option>
                                    <?php
                                    while ($paginas = mysql_fetch_assoc($b_paginas)) {
                                        ?>
                                        <option value="<?php echo $paginas["link"]; ?>"><?php echo utf8_encode($paginas["nombre"]); ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" align="center"><input name="guardar" id="guardar" type="submit"
                                                                  value="Guardar" class="botontextarea"></td>
                        </tr>
                    </table>
                </form>
                <hr>
                <form action="subboton_ctrl.php?opc=2" method="post">
                    <table width="600" align="center" bordercolor="#FFFFFF">
                        <tr>
                            <td width="200" align="center" class="fuente_texto11"><strong>Submenu</strong></td>
                            <td width="200" align="center" class="fuente_texto11"><strong>Menu</strong></td>
                            <td width="200" align="center" class="fuente_texto11"><strong>P&aacute;gina</strong></td>
                            <td width="200" align="center" class="fuente_texto11"><strong>-</strong></td>
                        </tr>
                        <?php
                        $i=0;
                        while ($rowSub = mysql_fetch_assoc($b_submenu)) {
                            $botonesSql_2 = mysql_query("SELECT * FROM `menu` where estado = 1 order by id asc", $dbh) or die(mysql_error());
                            $b_paginas_2 = mysql_query("select * from paginas order by nombre", $dbh) or die(mysql_error());
                            ?>
                            <input name="idboton<? echo $i?>" type="hidden" value="<? echo $rowSub["id"]; ?>" />
                            <tr>
                                <td width="200" align="center"
                                    class="fuente_texto55"><input name="nombre_sub<? echo $i ?>" type="text"
                                                                  value="<? echo utf8_encode($rowSub["nombre"]) ?>"
                                                                  class="textare-estilo"></td>
                                <td width="200" align="center" class="fuente_texto55">
                                    <select name="id_menu_upd<? echo $i ?>" class="textare-estilo2">
                                        <?php
                                        while ($rowMenu = mysql_fetch_assoc($botonesSql_2)) {
                                            ?>
                                            <option value="<?php echo $rowMenu['id'] ?>"<?php if ($rowSub['id_menu'] == $rowMenu['id']) {
                                                echo "selected";
                                            } else {
                                                echo "";
                                            } ?>><?php echo utf8_encode($rowMenu['boton']) ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td width="200" align="center" class="fuente_texto55">
                                    <select name="pag_upd<? echo $i ?>" class="textare-estilo2">
                                        <option value="#"<? if ($rowSub["link"] == "" || $rowSub['link'] == "#") {
                                            echo "selected";
                                        } ?>>Sin P&aacute;gina
                                        </option>
                                        <option value="index.php" <? if ($rowSub["link"] == "index.php") {
                                            echo "selected";
                                        } ?>>Inicio
                                        </option>
                                        <option value="contacto.php" <? if ($rowSub["link"] == "contacto.php") {
                                            echo "selected";
                                        } ?>>Contacto
                                        </option>
                                        <?php
                                        while ($rowPag = mysql_fetch_assoc($b_paginas_2)) {
                                            ?>
                                            <option value="<?php echo $rowPag['link'] ?>"<?php if ($rowSub['link'] == $rowPag['link']) {
                                                echo "selected";
                                            } else {
                                                echo "";
                                            } ?>><?php echo utf8_encode($rowPag['nombre']) ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td width="200" align="center" class="fuente_textotrick"><a class="fuente_texto22"
                                                                                            onclick="borrarLink('<?php echo $rowSub['id'] ?>')"
                                                                                            style="cursor: pointer;">Eliminar</a>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                        ?>
                        <input name="max" value="<?php echo $i?>" type="hidden" />
                        <tr>
                            <td align="center" valign="middle" colspan="6"><input name="refresh2" type="submit" class="botontextarea" value ="Actualizar" /></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function validaForm() {
            var nombre = document.getElementById('nombre').value;
            var menuprincipal = document.getElementById('menu_principal').value;
            var pagina = document.getElementById('pagina').value;
            if (nombre.length > 0 && menuprincipal.length > 0 && pagina.length > 0) {
                return true;
            } else {
                if (nombre.length == 0) {
                    alert("Debe ingresar un nombre");
                } else if (menuprincipal.length == 0) {
                    alert("Debe seleccionar el menu principal asociado");
                } else if (pagina.length == 0) {
                    alert("Debe seleccionar una pagina para asociar");
                }
                return false;
            }
        }

        function borrarLink(id) {
            if (confirm('¿Está seguro de eliminar esta relación?')) {
                document.location = "subboton_ctrl.php?opc=3&id=" + id;
            }
        }
    </script>
<? include("footer.php"); ?>