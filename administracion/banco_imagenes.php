<?php 
include("head.php");
$sql = mysql_query("select * from banco_imagenes order by id") or die(mysql_error());
?>
<script type="text/javascript">
function verImagen(id){
    $(document).ready(function (){
        $.fancybox({
            'width': 165,
            'height': 208,
            'align': 'center',
            'autoSize': true,
            'transitionIn': 'fade',
            'transitionOut': 'fade',
            'centerOnScroll': true,
            'type': 'iframe',
            'href': "frameBancoImagen.php?id="+id,
            //'onClosed' : function(){location.href = 'detalle_carro.php';}
        });
    }); 
}
function validarForm(){
    if(document.subir.archivo.value.length!=0){
        document.subir.submit();
    }else{
        alert("Debe seleccionar un archivo para cargar");
    }
}
function controlLogo(f){
	var ext=['jpg','png','gif'];
	var v=f.value.split('.').pop().toLowerCase();
	for(var i=0,n;n=ext[i];i++){
		if(n.toLowerCase()==v)
			return
	}
	var t=f.cloneNode(true);
	t.value='';
	f.parentNode.replaceChild(t,f);
	alert('Extensi\u00f3n no v\u00e1lida, solo archivos PNG, JPG o GIF');
}
</script>
<div id="main">
	<div class="wrapper">
    	<div id="content">
        	<div id="page-title">
            	<span class="title">Im&aacute;genes para sus productos</span>
                <span class="subtitle">Opple Chile</span>
			</div>
            <h4 align="center">Agregar nueva im&aacute;gen</h4>
            <form name="subir" action="banco_imagenes_ctrl.php" method="post" enctype="multipart/form-data">
            <table align="center">
              <tr>
                <td align="center" colspan="2"><p></p></td>
              </tr>
              <tr>
              	<td align="center" colspan="2" style="font-size:10px;">Por su seguridad evite subir archivos que puedan comprometer la seguridad de su sitio(Ejemplo, archivos .exe) <br/>
                Resoluci&oacute;n recomendada 125 x 148 p&iacute;xeles<br />
                <input name="archivo" type="file" class="textare-estilo" onchange="controlLogo(this)" />
                </td>
              </tr>
              <tr id="tr_2">
        		<td height="40" align="center" class="texto-pequeno" colspan="2">Tama&ntilde;o m&aacute;x. 2MB</td>
              </tr>
              <tr>
              	<td colspan="2" align="center"><input name="continuar" type="button" class="botontextarea" value="Subir archivo" onclick="validarForm()"></td>
              </tr>
            </table>
            </form>
            <br />
            <table width="600" align="center" bordercolor="#FFFFFF">
              <tr>
              	<td width="300" align="center" class="fuente_texto11"><strong>Imagen</strong></td>
                <td width="300" align="center" class="fuente_texto11"><strong>Tareas</strong></td>
              </tr>
              <?php
              if(mysql_num_rows($sql)>0){
                while($archivos = mysql_fetch_assoc($sql)){
                ?>
				  <tr>
					<td width="300" align="left" class="fuente_texto55"><? echo $archivos["imagen"]?></td>
					<td width="300" align="center" class="fuente_textotrick">
                    <a class="fuente_texto22" onclick="verImagen('<?php echo $archivos['id']?>')" style="cursor: pointer;">Ver</a></td>
				  </tr>
			    <?php
                }
            }else{
                ?>
                <tr>
                <td align="center" class="fuente_texto55" colspan="2">No registra archivos cargados.</td>
              </tr>
            <?php
            }?>
            </table>
		</div>
	</div>
</div>
<?php
include("footer.php");
?>
<?php
//alert=ok&tip=1
if($_GET['alert']=='ok'){
    if($_GET['tip']=='1'){
        $msg = "Im\u00e1gen cargada a su servidor";
    }
    echo '<script>alert("'.utf8_encode($msg).'");</script>';
}
if($_GET['err']=="archivo"){
    switch($_GET['tip']){
        case "0":$msg=utf8_encode("Error al cargar im\u00e1gen, contactarse con el administrador");break;
        case "weight":$msg=utf8_encode("Error al cargar im\u00e1gen, archivo supera el peso m\u00e1ximo permitido");break;
    }
    echo '<script>alert("'.$msg.'");</script>';
}
?>