<?php
include("head.php");
?>
    <script type="text/javascript">
        function validaForm() {
            var nombrePagina = document.crear.nombre.value.length;
            if (nombrePagina != 0) {
                document.crear.submit();
            } else {
                alert("El nombre de su pagina es obligatorio");
                document.crear.nombre.focus();
            }
        }

        function controlLogo(f) {
            var ext = ['jpg', 'png', 'gif'];
            var v = f.value.split('.').pop().toLowerCase();
            for (var i = 0, n; n = ext[i]; i++) {
                if (n.toLowerCase() == v)
                    return
            }
            var t = f.cloneNode(true);
            t.value = '';
            f.parentNode.replaceChild(t, f);
            alert('Extensi\u00f3n no v\u00e1lida, solo archivos PNG, JPG o GIF');
        }

    </script>
    <div id="main">
        <div class="wrapper">
            <div id="content">
                <div id="page-title">
                    <span class="title">Crear P&aacute;gina</span>
                    <span class="subtitle">Opple Chile</span>
                </div>
                <h4 align="center">&nbsp;</h4>
                <form name="crear" action="crear_ctrl.php" method="post">
                    <table width="578" align="center">
                        <tr>
                            <td align="center" colspan="2"><span
                                        class="textoadministrador">Nombre de su p&aacute;gina </span><br><input
                                        name="nombre" size="50" maxlength="255"
                                        style="font-family:Verdana, Geneva, sans-serif" type="text"
                                        class="textare-estilo" value=""/></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2"><p></p></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2"><span class="textoadministrador">Contenido Superior</span><br/>
                                <textarea name="contenido_top" rows="15" cols="80" style="width: 80%"
                                          class="tinymce"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><hr></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2"><span class="textoadministrador">Contenido Principal</span><br/>
                                <textarea name="contenido" rows="15" cols="80" style="width: 80%"
                                          class="tinymce"></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">Tipo:
                                <select name="tipo" class="textare-estilo2">
                                    <option value="1">Ancho completo</option>
                                    <option value="2">Con men&uacute; lateral</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center"><input name="continuar" type="button" class="botontextarea"
                                                                  value="Crear" onclick="validaForm()"></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
<?php
include("footer.php");
?>
<?php
if ($_GET['pag'] == "err") {
    $msg = utf8_encode("Error al crear pagina, ya existe una con este nombre");
    echo '<script>alert("' . $msg . '");</script>';
} else if ($_GET['pag'] == "bann") {
    $msg = utf8_encode("Error al subir banner de su pagina, el peso del archivo supera el permitido");
    echo '<script>alert("' . $msg . '");</script>';
} else if ($_GET['pag'] == "0") {
    $msg = utf8_encode("Ocurrio un error al crear su pagina, contactese con el administrador: framos@3amweb.cl");
    echo '<script>alert("' . $msg . '");</script>';
}
?>