<?php
include("head.php");
$busca_categorias = mysql_query("select s.id, s.descripcion as subfamilia, f.descripcion as familia, c.descripcion as categoria from subfamilia s
inner join familia f on f.id = s.id_familia
inner join categorias c on c.id = f.id_categoria") or die(mysql_error());
$id = $_GET['id'];
$datosProductos = mysql_query("select * from productos where id = " . $id) or die(mysql_error());
$rowProducto = mysql_fetch_assoc($datosProductos);
$checkOferta = "";
$checkDestacado = "";
$checkMasVendido = "";
$checkStock = "";
if ($rowProducto['es_oferta'] == 1) {
    $checkOferta = "checked";
}
if ($rowProducto['es_destacado'] == 1) {
    $checkDestacado = "checked";
}
if ($rowProducto['es_masvendido'] == 1) {
    $checkMasVendido = "checked";
}
if ($rowProducto['stock'] == 0) {
    $checkStock = "checked";
}
$buscaImagen = mysql_query("select * from banco_imagenes where id = '" . $rowProducto['imagen'] . "'") or die(mysql_error());
$rowImagen = mysql_fetch_assoc($buscaImagen);

$buscaPDF = mysql_query("select * from banco_fichas where id = '" . $rowProducto['ficha_tecnica'] . "'") or die(mysql_error());
$rowPDF = mysql_fetch_assoc($buscaPDF);

$buscaQR = mysql_query("select * from banco_qr where id = '" . $rowProducto['qr'] . "'") or die(mysql_error());
$rowQR = mysql_fetch_assoc($buscaQR);
?>
    <script type="text/javascript">
        function valida() {
            var codigo = document.crear.codigo;
            var nombre = document.crear.nombre;
            var precio = document.crear.precio;
            var imagen = document.crear.imagen;
            if (codigo.value.length != 0 && nombre.value.length != 0 && precio.value.length != 0 && imagen.value.length != 0) {
                document.crear.submit();
            } else {
                if (codigo.value.length == 0) {
                    alert("Debe ingresar el codigo del producto");
                    codigo.focus();
                } else if (nombre.value.length == 0) {
                    alert("Debe ingresar el nombre del producto");
                    nombre.focus();
                } else if (precio.value.length == 0) {
                    alert("Debe ingresar el precio del producto");
                    precio.focus();
                } else if (imagen.value.length == 0) {
                    alert("Debe seleccionar una imagen para el producto");
                }
            }
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function seleccionarImagen() {
            $(document).ready(function () {
                $.fancybox({
                    'width': 430,
                    'height': 620,
                    'align': 'center',
                    'autoSize': true,
                    'transitionIn': 'fade',
                    'transitionOut': 'fade',
                    'centerOnScroll': true,
                    'type': 'iframe',
                    'href': "frameImagen.php",
                    //'onClosed' : function(){location.href = 'detalle_carro.php';}
                });
            });
        }

        function seleccionarQR() {
            $(document).ready(function () {
                $.fancybox({
                    'width': 430,
                    'height': 620,
                    'align': 'center',
                    'autoSize': true,
                    'transitionIn': 'fade',
                    'transitionOut': 'fade',
                    'centerOnScroll': true,
                    'type': 'iframe',
                    'href': "frameQr.php",
                    //'onClosed' : function(){location.href = 'detalle_carro.php';}
                });
            });
        }

        function seleccionarFicha() {
            $(document).ready(function () {
                $.fancybox({
                    'width': 430,
                    'height': 620,
                    'align': 'center',
                    'autoSize': true,
                    'transitionIn': 'fade',
                    'transitionOut': 'fade',
                    'centerOnScroll': true,
                    'type': 'iframe',
                    'href': "frameFichas.php",
                    //'onClosed' : function(){location.href = 'detalle_carro.php';}
                });
            });
        }

        function quitar(tipo) {
            if (tipo == 'qr') {
                document.getElementById('qr').value = '';
                document.getElementById('id_qr').value = '';
            } else if (tipo == 'ficha') {
                document.getElementById('ficha').value = '';
                document.getElementById('id_ficha').value = '';
            }
        }
    </script>
    <div id="main">
        <div class="wrapper">
            <div id="content">
                <div id="page-title">
                    <span class="title">Editar Productos</span>
                    <span class="subtitle">Opple Chile</span>
                </div>
                <h4 align="center">Datos Principales del Producto</h4>
                <form name="crear"
                      action="update_productos_ctrl.php?id=<?php echo $id ?>&idf=<?php echo $_GET['idf'] ?>"
                      method="post" enctype="multipart/form-data">
                    <table width="900" align="center" bordercolor="#FFFFFF">
                        <tr>
                            <td width="128" align="center" class="fuente_texto11"><strong>C&oacute;digo
                                    Producto</strong></td>
                            <td width="128" align="center" class="fuente_texto11"><strong>Nombre Producto</strong></td>
                            <td width="128" align="center" class="fuente_texto11"><strong>Precio</strong></td>
                            <td width="128" align="center" class="fuente_texto11"><strong>Subfamilia Asociada</strong>
                            </td>
                            <td width="128" align="center" class="fuente_texto11"><strong>Oferta?</strong></td>
                            <td width="128" align="center" class="fuente_texto11"><strong>Destacado?</strong></td>
                            <td width="128" align="center" class="fuente_texto11"><strong>M&aacute;s vendido?</strong>
                            </td>
                            <td width="128" align="center" class="fuente_texto11"><strong>Agotado?</strong></td>
                        </tr>
                        <tr>
                            <td width="128" align="center"><input name="codigo"
                                                                  value="<?php echo $rowProducto['codigo_producto'] ?>"
                                                                  type="text" style="width: 150px;"/></td>
                            <td width="128" align="center"><input name="nombre"
                                                                  value="<?php echo utf8_encode($rowProducto['nombre']) ?>"
                                                                  type="text" style="width: 150px;"/></td>
                            <td width="128" align="center"><input name="precio"
                                                                  value="<?php echo $rowProducto['precio'] ?>"
                                                                  type="text" style="width: 150px;"
                                                                  onkeypress="javascript:return isNumberKey(event)"
                                                                  maxlength="8"/></td>
                            <td width="128" align="center"><select name="subfamilia" style="width: 150px;">
                                    <?php while ($row = mysql_fetch_assoc($busca_categorias)) {
                                        ?>
                                        <option value="<?php echo $row['id'] ?>" <?php if ($row['id'] == $rowProducto['id_subfamilia']) {
                                            echo "selected";
                                        } ?>><?php echo utf8_encode($row['categoria'] . "-" . $row['familia'] . "-" . $row['subfamilia']) ?></option>
                                        <?php
                                    } ?>
                                </select></td>
                            <td width="128" align="center"><input name="oferta" value="1"
                                                                  type="checkbox" <?php echo $checkOferta; ?>/></td>
                            <td width="128" align="center"><input name="destacado" value="1"
                                                                  type="checkbox" <?php echo $checkDestacado; ?>/></td>
                            <td width="128" align="center"><input name="mas_vendido" value="1"
                                                                  type="checkbox" <?php echo $checkMasVendido; ?>/></td>
                            <td width="128" align="center"><input name="stock" value="1"
                                                                  type="checkbox" <?php echo $checkStock; ?>/></td>
                        </tr>
                    </table>
                    <hr/>
                    <h4 align="center">Atributos del producto</h4>
                    <table width="700" align="center">
                        <tr>
                            <td align="right" width="300"><input name="sel" onclick="seleccionarImagen()"
                                                                 value="Seleccionar Imagen de Producto" type="button"
                                                                 class="botontextarea"/></td>
                            <td width="300"><input name="imagen" id="imagen" value="<?php echo $rowImagen['imagen'] ?>"
                                                   type="text" disabled="disabled" style="width: 300px;"/></td>
                            <input name="id_imagen" id="id_imagen" value="<?php echo $rowProducto['imagen'] ?>"
                                   type="hidden"/>
                        </tr>
                        <tr>
                            <td align="right" width="300"><input name="sel" onclick="seleccionarFicha()"
                                                                 value="Seleccionar Ficha Técnica" type="button"
                                                                 class="botontextarea"/></td>
                            <td width="300"><input name="ficha" id="ficha" value="<?php echo $rowPDF['imagen'] ?>"
                                                   type="text" disabled="disabled" style="width: 300px;"/>
                                <img src="imagenes/menos.jpg" alt="Quitar Ficha Ténica" title="Quitar Ficha Técnica"
                                     style="vertical-align: middle; cursor: pointer;" onclick="quitar('ficha')">
                            </td>
                            <input name="id_ficha" id="id_ficha" value="<?php echo $rowProducto['ficha_tecnica'] ?>"
                                   type="hidden"/>
                        </tr>
                        <tr>
                            <td align="right" width="300"><input name="sel" onclick="seleccionarQR()"
                                                                 value="Seleccionar QR" type="button"
                                                                 class="botontextarea"/></td>
                            <td width="300"><input name="qr" id="qr" value="<?php echo $rowQR['imagen'] ?>" type="text"
                                                   disabled="disabled" style="width: 300px;"/>
                                <img src="imagenes/menos.jpg" alt="Quitar Código QR" title="Quitar Código QR"
                                     style="vertical-align: middle; cursor: pointer;" onclick="quitar('qr')">
                            </td>
                            <input name="id_qr" id="id_qr" value="<?php echo $rowProducto['qr'] ?>" type="hidden"/>
                        </tr>
                        <?php
                        $b_atributos = mysql_query("select * from atributos order by id") or die(mysql_error());
                        $i = 0;
                        while ($rowAtributo = mysql_fetch_assoc($b_atributos)) {
                            $batributo = mysql_query("select valor from atributos_producto where id_producto = " . $id . " and id_atributo = " . $rowAtributo['id'] . "") or die(mysql_error());
                            $rowap = mysql_fetch_assoc($batributo);
                            ?>
                            <tr>
                                <td width="300" align="right">
                                    <strong><?php echo utf8_encode($rowAtributo['descripcion']) ?></strong></td>
                                <td width="300"><input name="atributo_<?php echo $i; ?>" type="text"
                                                       value="<?php echo utf8_encode($rowap['valor']) ?>"
                                                       style="width: 300px;"/><input
                                            name="id_atributo_<?php echo $i; ?>" type="hidden"
                                            value="<?php echo $rowAtributo['id'] ?>" style="width: 300px;"/></td>
                            </tr>
                            <?php
                            $i++;
                        } ?>
                        <tr>
                            <td colspan="2"><p><input name="total" value="<?php echo $i ?>" type="hidden"/></p></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2"><input name="crear" value="Actualizar Producto" type="button"
                                                                  class="botontextarea" onclick="valida()"/></td>
                        </tr>
                    </table>
                    <br/>
                </form>
            </div>
        </div>
    </div>
<?php
include("footer.php");
?>