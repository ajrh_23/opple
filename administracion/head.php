<?php
include("seguridad.php");
include("conect.php"); 
?>
<!DOCTYPE  html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Opple Chile</title>
		<!-- CSS -->
		<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
		<!--[if IE 8]>
			<link rel="stylesheet" type="text/css" media="screen" href="css/ie8-hacks.css" />
		<![endif]-->
		<!-- ENDS CSS -->	
		<!-- JS -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.13.custom.min.js"></script>
		<script type="text/javascript" src="js/vanadium.js"></script>
		<script type="text/javascript" src="js/easing.js"></script>
		<script type="text/javascript" src="js/jquery.scrollTo-1.4.2-min.js"></script>
		<script type="text/javascript" src="js/jquery.cycle.all.js"></script>
		
		<!-- Isotope -->
		<script src="js/jquery.isotope.min.js"></script>
		
		<!--[if IE]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		<!--[if IE 6]>
			<script type="text/javascript" src="js/DD_belatedPNG.js"></script>
			<script>
	      		/* EXAMPLE */
	      		//DD_belatedPNG.fix('*');
	    	</script>
		<![endif]-->
		<!-- ENDS JS -->
		
		
		<!-- Nivo slider -->
		<link rel="stylesheet" href="css/nivo-slider.css" type="text/css" media="screen" />
		<script src="js/nivo-slider/jquery.nivo.slider.js" type="text/javascript"></script>
		<!-- ENDS Nivo slider -->
		
		<!-- tabs -->
		<link rel="stylesheet" href="css/tabs.css" type="text/css" media="screen" />
		<script type="text/javascript" src="js/tabs.js"></script>
  		<!-- ENDS tabs -->
  		
  		<!-- prettyPhoto 
		<script type="text/javascript" src="js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
		<link rel="stylesheet" href="js/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" />
		<!-- ENDS prettyPhoto -->
		
		<!-- superfish -->
		<link rel="stylesheet" media="screen" href="css/superfish.css" /> 
		<link rel="stylesheet" media="screen" href="css/superfish-left.css" /> 
		<script type="text/javascript" src="js/superfish-1.4.8/js/hoverIntent.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/superfish.js"></script>
		<script type="text/javascript" src="js/superfish-1.4.8/js/supersubs.js"></script>
		<!-- ENDS superfish -->
		
		<!-- poshytip -->
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-twitter/tip-twitter.css" type="text/css" />
		<link rel="stylesheet" href="js/poshytip-1.0/src/tip-yellowsimple/tip-yellowsimple.css" type="text/css" />
		<script type="text/javascript" src="js/poshytip-1.0/src/jquery.poshytip.min.js"></script>
		<!-- ENDS poshytip -->
		
		<!-- Tweet -->
		<link rel="stylesheet" href="css/jquery.tweet.css" media="all"  type="text/css"/> 
		<script src="js/tweet/jquery.tweet.js" type="text/javascript"></script> 
		<!-- ENDS Tweet -->
		
		<!-- Fancybox -->
		<script type="text/javascript" src="../fb/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="../fb/jquery.fancybox-1.3.4.pack.js"></script>
        <link rel="stylesheet" type="text/css" href="../fb/jquery.fancybox-1.3.4.css" media="screen" /> 
		<!-- ENDS Fancybox -->
<!-- Load TinyMCE -->
<script type="text/javascript" src="jscripts/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : 'jscripts/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",

			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",

			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
	});
</script>
<!-- /TinyMCE -->
	</head>
<style>
.centro{
	text-align:center;
	height: 125px;
	padding-top: 12px;
}
</style>
<div id="header" class="centro">
<img src="images/logo.png"></div>
			<div id="menu">
				<!-- ENDS menu-holder -->
				<div id="menu-holder">
					<!-- wrapper-menu -->
					<div class="wrapper">
						<!-- Navigation -->
						<ul id="nav" class="sf-menu">
                            <li id="menu"><a href="#">Paginas<span class="subheader">P&aacute;ginas</span></a>
                                <ul>
                                    <li><a href="home.php"><span>Home</span></a></li>
                                    <li id="submenu"><a href="#"><span>Crear P&aacute;gina</span></a>
                                    	<ul>
                                        	<li><a href="crear_pagina.php"><span>Crear P&aacute;gina</span></a></li>
                                        	<li><a href="b&g_pag.php"><span>Busqueda y Gesti&oacute;n</span></a></li>
                                        </ul>
                                    </li>
                                    <!--<li><a href="encabezado.php"><span>Encabezado</span></a></li>-->
                                    <li><a href="pie.php"><span>Pie de Pagina y Contacto</span></a></li>
                              </ul>
							</li>
                            <li id="menu"><a href="#">Productos<span class="subheader">Productos</span></a>
                                <ul>
                                    <li><a href="categorias.php"><span>Categor&iacute;as</span></a></li>
                                    <li id="submenu"><a href="#"><span>Familias</span></a>
                                        <ul>
                                            <li><a href="familias.php"><span>Crear Familia</span></a></li>
                                            <li><a href="familias_b&g.php"><span>Editar Familias</span></a></li>
                                        </ul>
                                    </li>
                                    <li id="submenu"><a href="#"><span>Sub-Familias</span></a>
                                        <ul>
                                            <li><a href="subfamilias.php"><span>Crear Sub-Familia</span></a></li>
                                            <li><a href="subfamilias_b&g.php"><span>Editar Sub-Familias</span></a></li>
                                        </ul>
                                    </li>
                                    <li id="submenu"><a href="#"><span>Productos</span></a>
                                        <ul>
                                            <li><a href="productos.php"><span>Crear Producto</span></a></li>
                                            <li><a href="productos_b&g_p1.php"><span>Editar Productos</span></a></li>
                                        </ul>
                                    </li>
                                    <li><a href="banco_imagenes.php"><span>Im&aacute;genes para sus productos</span></a></li>
                                    <li><a href="banco_fichas.php"><span>Fichas Técnicas para sus productos</span></a></li>
                                    <li><a href="banco_qr.php"><span>Códigos QR para sus productos</span></a></li>
                                </ul>
							</li>
                            <li id="menu"><a href="#">Menu<span class="subheader">Men&uacute;</span></a>
                                <ul>
                                    <li><a href="menu.php"><span>Men&uacute; Principal</span></a></li>
                                    <li><a href="submenu.php"><span>Sub men&uacute;</span></a></li>
                                </ul>
                            </li>
                            <li id="menu"><a href="subir.php">Subir Archivos<span class="subheader">Subir Archivos</span></a></li>
                            <li id="menu"><a href="vendedores.php">Vendedores<span class="subheader">Vendedores</span></a></li>
                            <li id="menu"><a href="close.php">Cerrar Sesi&oacute;n<span class="subheader"> Cerrar Sesi&oacute;n</span></a></li>
						</ul>
						<!-- Navigation -->
					</div>
					<!-- wrapper-menu -->
				</div>
				<!-- ENDS menu-holder -->
			</div>
			<!-- ENDS Menu -->