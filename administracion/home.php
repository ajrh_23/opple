<?php
include("head.php");
$query = "select * from home";
try {
    $sql = mysql_query($query);
    $row = mysql_fetch_assoc($sql);
    $titulo = $row['titulo'];
    $contenido = $row['contenido'];
} catch (Exception $e) {
    echo "Error " . $e . ", contactar con administrador 3amweb framos@3amweb.cl";
}
?>
    <script type="text/javascript">
        function controlBanner(f) {
            var ext = ['swf'];
            var v = f.value.split('.').pop().toLowerCase();
            for (var i = 0, n; n = ext[i]; i++) {
                if (n.toLowerCase() == v)
                    return
            }
            var t = f.cloneNode(true);
            t.value = '';
            f.parentNode.replaceChild(t, f);
            alert('Extensi\u00f3n no v\u00e1lida, solo archivos SWF');
        }
    </script>
    <div id="main">
        <h4 align="center">&nbsp;</h4>
        <form name="foot" action="home_ctrl.php" method="post">
            <table width="578" align="center">
                <tr>
                    <td align="center" colspan="2"><span class="textoadministrador">Contenido Principal</span><br/>
                        <textarea name="contenido" rows="15" cols="80" style="width: 80%"
                                  class="tinymce"><?php echo $contenido; ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input name="continuar" type="submit" class="botontextarea"
                                                          value="Actualizar"></td>
                </tr>
            </table>
        </form>
    </div>
<?php
include("footer.php");
?>
<?php
if ($_GET['err']) {
    switch ($_GET['err']) {
        case "0":
            $msg = utf8_encode("Error al cargar banner, contactarse con el administrador");
            break;
        case "weight":
            $msg = utf8_encode("Error al cargar banner, archivo supera el peso maximo permitido");
            break;
        case "ok":
            $msg = utf8_encode("Actualizacion correcta!");
            break;
    }
    echo '<script>alert("' . $msg . '");</script>';
}
?>