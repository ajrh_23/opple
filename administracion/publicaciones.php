<?php 
include("head.php");
?>
<script type="text/javascript">
function controlBanner(f){
	var ext=['swf'];
	var v=f.value.split('.').pop().toLowerCase();
	for(var i=0,n;n=ext[i];i++){
		if(n.toLowerCase()==v)
			return
	}
	var t=f.cloneNode(true);
	t.value='';
	f.parentNode.replaceChild(t,f);
	alert('Extensi\u00f3n no v\u00e1lida, solo archivos SWF');
}
function validarForm(){
    if(document.getElementById('banner').value.length!=0){
        document.publicacion.action="publicaciones_ctrl.php?opc=1";
        document.publicacion.submit();
    }else{
        alert("Debe seleccionar un archivo para cargar");
    }
}
</script>
<?php
$query = mysql_query("select * from eventos") or die(mysql_error());
$row = mysql_fetch_assoc($query);
$contenido = $row['contenido'];
?>
<div id="main">
	<div class="wrapper">
    	<div id="content">
        	<div id="page-title">
            	<span class="title">Publicaciones</span>
                <span class="subtitle">CENTRO DE CONSTRUCCI&Oacute;N CHINA</span>
			</div>
            <h4 align="center">&nbsp;</h4>
            <form name="publicacion" action="publicaciones_ctrl.php?opc=1" method="post" enctype="multipart/form-data">
            <table width="578" align="center">
              <tr>
                <td align="center" colspan="2"><p></p></td>
              </tr>
              <tr>
              	<td align="center" colspan="2"><span class="textoadministrador">Contenido Eventos (Panel derecho de su p&aacute;gina)</span><br /> 
                  <textarea name="contenido" rows="15" cols="80" style="width: 80%" class="tinymce"><?php echo $contenido;?></textarea>
                </td>
              </tr>
              <tr>
              	<td colspan="2" align="center"><input name="continuar" type="submit" class="botontextarea" value="Actualizar"></td>
              </tr>
            </table>
            </form>
		</div>
	</div>
</div>
<?php
include("footer.php");
?>
<?php
if($_GET['err']=="banner"){
    switch($_GET['tip']){
        case "0":$msg=utf8_encode("Error al cargar archivo, contactarse con el administrador");break;
        case "weight":$msg=utf8_encode("Error al cargar, archivo supera el peso máximo permitido");break;
    }
    echo '<script>alert("'.$msg.'");</script>';
}else if($_GET['alert']=='ok'){
    switch($_GET['tip']){
        case "1":$msg=utf8_encode("Eventos Actualizados");break;
        case "2":$msg=utf8_encode("Publicación desactivada");break;
        case "3":$msg=utf8_encode("Publicación activada");break;
        case "4":$msg=utf8_encode("Publicación eliminada");break;
    }
    echo '<script>alert("'.$msg.'");</script>';
}
?>