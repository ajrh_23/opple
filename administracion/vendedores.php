<?php 
include("head.php");
?>
<script type="text/javascript">
    function crear() {
        var user = document.getElementById('user').value;
        var nombre = document.getElementById('nombre').value;
        var pass = document.getElementById('pass').value;
        if(user.length > 0 && nombre.length > 0 && pass.length > 0) {
            document.getElementById('crearform').submit();
        }else {
            if(user.length == 0) {
                alert("Debe ingresar un nombre usuario");
                document.getElementById('user').focus();
            }else if(nombre.length == 0) {
                alert("Debe ingresar un Nombre");
                document.getElementById('nombre').focus();
            }else if(pass.length == 0) {
                alert("Debe ingresar una clave de acceso");
                document.getElementById('pass').focus();
            }
        }
    }
</script>
<div id="main">
	<div class="wrapper">
    	<div id="content">
        	<div id="page-title">
            	<span class="title">Gesti&oacute;n de Vendedores</span>
                <span class="subtitle">Opple Chile</span>
			</div>
            <form name="crearform" id="crearform" action="vendedoresadd_ctrl.php" method="post">
                <table width="900" align="center" bordercolor="#FFFFFF">
                    <tr>
                        <td width="225" align="center" class="fuente_texto11"><strong>Usuario de acceso</strong></td>
                        <td width="225" align="center" class="fuente_texto11"><strong>Nombre</strong></td>
                        <td width="225" align="center" class="fuente_texto11"><strong>Clave</strong></td>
                        <td width="225" align="center" class="fuente_texto11"><strong>-</strong></td>
                    </tr>
                    <tr>
                        <td align="center"><input name="user" id="user" type="text" value="" /></td>
                        <td align="center"><input name="nombre" id="nombre" type="text" value="" /></td>
                        <td align="center"><input name="pass" id="pass" type="text" value="" /></td>
                        <td align="center"><input type="button" onclick="crear()" value="Crear" class="botontextarea" /> </td>
                    </tr>
                </table>
            </form>
            <form action="vendedoresupd_ctrl.php" method="post">
            <table width="900" border="0" align="center" bordercolor="#FFFFFF">
            <tr>
            <td height="30" valign="top"><i class="textoadministrador"><strong>Vendedores</strong></i></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
                <tr>
                    <td align="center">Usuario</td>
                    <td align="center">Nombre</td>
                    <td align="center">Clave</td>
                    <td align="center">Activado/Desactivado</td>
                </tr>
            <? 
            $busca_botones = mysql_query("SELECT * FROM `seguridad` where auth = 'cotizaciones' and tipo = 1 order by idu asc",$dbh) or die(mysql_error());
            $i=0;
            while($boton = mysql_fetch_assoc($busca_botones)){
            	switch($boton["estado"]){
            	   case "0":$estado="Desactivado";$check=""; break;
            	   case "1":$estado="Activado";$check="checked"; break;
            	break;
            	} 
            	?>
            	<input name="idusuario<? echo $i?>" type="hidden" value="<? echo $boton["idu"]; ?>" />
            	<tr>
            	  	<td width="225" align="center"><input name="user<? echo $i?>" type="text" value="<?php echo utf8_encode($boton['user'])?>" class="textare-estilo7" /></td>
                    <td width="225" align="center"><input name="nombre<? echo $i?>" type="text" value="<?php echo utf8_encode($boton['nombre'])?>" class="textare-estilo7" /></td>
                    <td width="225" align="center"><input name="pass<? echo $i?>" type="text" value="<?php echo utf8_encode($boton['pass'])?>" class="textare-estilo7" /></td>
            		<td width="225" align="center"><input name="estado<? echo $i?>" type="checkbox" value="1"<? echo $check?>></td>
            	</tr>
            	<? 
            	$i = $i+1;
            }?>
            <input name="max" value="<?php echo $i?>" type="hidden" />
            <tr>
                <td align="center" valign="middle" colspan="6"><input name="refresh2" type="submit" class="botontextarea" value ="Actualizar" /></td>
              </tr>
            </table>
            </form>
				  </div>
				</div>
			</div>	
<? include("footer.php"); ?>