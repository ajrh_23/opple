$(document).ready(function () {

	var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;

	var num = /^[0-9]+$/;

	$("#boton").click(function (){

		$(".error").remove();		

		if( $("#Nombre").val() == "" ){

		$("#Nombre").focus().after("<span class='error'>*Por favor corrija</span>");

		return false;

		}else if( $("#Email").val() == "" || !emailreg.test($("#Email").val()) ){

			$("#Email").focus().after("<span class='error'>*Por favor corrija</span>");

			return false;

		}else if( $("#telfijo").val() == "" || !num.test($("#telfijo").val()) ) {

			$("#telfijo").focus().after("<span class='error'>*Por favor corrija</span>");

			return false;

		}else if( $("#Mensaje").val() == "" ){

			$("#Mensaje").focus().after("<span class='error'>*Por favor corrija</span>");

			return false;

		}

	});

	$("#Nombre, #telfijo, #Mensaje").keyup(function(){

		if( $(this).val() != "" ){

			$(".error").fadeOut();			

			return false;

		}		

	});

	$("#Email").keyup(function(){

		if( $(this).val() != "" && emailreg.test($(this).val())){

			$(".error").fadeOut();			

			return false;

		}		

	});

		$("#telfijo").keyup(function(){

		if( $(this).val() != "" && num.test($(this).val())){

			$(".error").fadeOut();			

			return false;

		}		

	});

		$("#Telfijo").keyup(function(){

		if( $(this).val() != "" && Telfijoreg.test($(this).val())){

			$(".error").fadeOut();			

			return false;

		}		

	});

});