<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'c1textil_blog');

/** MySQL database username */
define('DB_USER', 'c1system_web');

/** MySQL database password */
define('DB_PASSWORD', 'am3r1c42015');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'S`[u;8(j,y(x{&E!M H-pNn7cVgVdyKfLecCbBeF/|G].ApmI]D?|{E9C#e/[)HJ');
define('SECURE_AUTH_KEY',  '=>J%6#!aM8SGsJ@F6VgANt(&!)G,mz^f3FC?#G|&`)U}{R4t3_V-kkXGvx+o}#/U');
define('LOGGED_IN_KEY',    '7-IV9Rr&,XZ*@POYcN|C(&jv,]a15tf-]|7~tB@^~%6)g5uU.S!4 A-BZr]>{CY.');
define('NONCE_KEY',        '8Y5*y2s OTla*,-tRlKDc$Q!h2Ak bN )$v&6$1EKfM#QYrzskiV+/)7+H&pGhE0');
define('AUTH_SALT',        '^}!PQ@q[dxuQAKk8glvrSxiA-e8<+Yn~Py-h^qq^YLc1SngA3&0/CI*AsP{EttKD');
define('SECURE_AUTH_SALT', 'J$85ev=[[Ssq[!BGXQ8s?%_EjL2=WdRI0q:n;Vl^EXPisEYX9IovMu@DJj/B/PkV');
define('LOGGED_IN_SALT',   'J:VCa|T 4HpueBnRIfivO_zZXh>W`6g*SGcK;Ejg:<0I?BMlAaO6};f^)&O(f5;M');
define('NONCE_SALT',       '<R%^Qx~ JIY4k f/1DtF4$++}g,hiM^Grn1[r+Z+C.GK#SI $bGXXbqKX(-SpaP/');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_opple';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
