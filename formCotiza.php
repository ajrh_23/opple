<?php

include "conect.php";

$queryRegiones = mysql_query("select * from region_cl order by id_re") or die(mysql_error());

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>OPPLE Chile - Cotizaci&oacute;n</title>
    <link href="css/contenedorinternas.css" rel="stylesheet" type="text/css"/>
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
    <style type="text/css">
        <!--
        body {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
        }

        .style1 {
            color: #B01B25
        }

        -->
    </style>
    <script src='funciones2.js'></script>
    <script type="text/javascript">
        function cargaComuna(id) {
            var xmlhttp;
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("comuna").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("POST", "comuna.php", true);
            xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xmlhttp.send("id=" + id);
        }
    </script>
</head>

<body>
<table border="0" align="center" cellpadding="0" cellspacing="0" style="width:100%;">
    <tr>
        <td align="right"><span class="close2" style="cursor: pointer; color: #FFF;"><img src="imagenes/close.png"
                                                                                          width="38"
                                                                                          height="38"/></span></td>
    </tr>
</table>
<table width="99" height="124" border="0" align="center" cellpadding="0" cellspacing="0" class="padding-cotizador">
    <tr>
        <td width="11" align="left" valign="top">
            <table width="500" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td height="42" align="center" valign="middle"><span class="titulo-detalle">IND&Iacute;QUENOS SUS DATOS PARA GENERAR COTIZACI&Oacute;N</span>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" bgcolor="#FFFFFF">
                        <table width="440" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="center">
                                    <form id="formulario" class="contacto" name="formulario" method="post" action="generacotizacion.php">
                                        <table width="440" border="0" align="left" cellpadding="0" cellspacing="0"
                                               bgcolor="#FFFFFF">

                                            <tr>
                                                <td height="25" colspan="2" align="left" valign="top"
                                                    class="texto-mail-cont">&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="28" align="left" valign="middle" class="texto-principal1">
                                                    Rut (<span class="style1">*</span>)
                                                </td>
                                                <td width="200" align="left" valign="top" class="texto-mail-cont">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="33" colspan="2" align="left" valign="top"
                                                    class="texto-politicas"><label></label>
                                                    <label>
                                                        <input name="rut" type="text" class="textarea-tel111" id="rut"/>
                                                    </label>
                                                    <span class="texto-global7">&nbsp;</span><span class="texto-ruta">(Ej: 12222334-3)</span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="28" colspan="2" align="left" valign="middle">
                                                    <table width="440" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="240" class="texto-principal1">Regi&oacute;n
                                                                (<span class="style1">*</span>)
                                                            </td>
                                                            <td width="200" class="texto-principal1">Comuna (<span
                                                                        class="style1">*</span>)
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="top">
                                                    <table width="440" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="240" height="33" align="left" valign="top">
                                                                <select name="region" class="textarea-tel111"
                                                                        id="region" onchange="cargaComuna(this.value)">
                                                                    <option>Selecione...</option>
                                                                    <?php
                                                                    while($reg = mysql_fetch_assoc($queryRegiones)){
                                                                        ?>
                                                                        <option value="<?php echo $reg['id_re']?>"><?php echo utf8_encode($reg['str_descripcion'])?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select></td>
                                                            <td width="200" height="33" align="left" valign="top">
                                                                <select name="comuna" class="textarea-tel111"
                                                                        id="comuna">
                                                                    <option>Seleccione...</option>
                                                                </select></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="28" colspan="2" align="left" valign="middle"
                                                    class="texto-principal1">Direcci&oacute;n (<span
                                                            class="style1">*</span>)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="top"><input name="direccion"
                                                                                                 type="text"
                                                                                                 tabindex="3"
                                                                                                 class="textarea-tel111"
                                                                                                 id="direccion"/></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="bottom">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="28" colspan="2" align="left" valign="middle">
                                                    <table width="440" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="255" class="texto-principal1">Empresa</td>
                                                            <td width="200" class="texto-principal1">Tel&eacute;fono
                                                                (<span class="style1">*</span>)
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="top">
                                                    <table width="440" border="0" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td width="150" height="33" align="left" valign="top"><input
                                                                        name="empresa" type="text" tabindex="3"
                                                                        class="textarea-tel111" id="empresa"/></td>
                                                            <td width="15" align="left" valign="top">&nbsp;</td>
                                                            <td width="180" height="33" align="left" valign="top"><input
                                                                        name="telefono" type="text"
                                                                        class="textarea-tel111" id="telefono"
                                                                        tabindex="3" size="12"/></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="28" colspan="2" align="left" valign="middle"
                                                    class="texto-principal1">Nombre (<span class="style1">*</span>)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="33" colspan="2" align="left" valign="top"><input
                                                            name="nombre" type="text" tabindex="3"
                                                            class="textarea-tel111" id="nombre"/></td>
                                            </tr>
                                            <tr align="left">
                                                <td height="28" colspan="2" valign="middle" class="texto-principal1">
                                                    E-mail (<span class="style1">*</span>)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="top"><input name="email"
                                                                                                 type="text"
                                                                                                 tabindex="4"
                                                                                                 class="textarea-tel111"
                                                                                                 id="email"/></td>
                                            </tr>
                                            <tr>
                                                <td height="30" colspan="2" align="left" valign="top"
                                                    class="texto-global7"><span class="texto-ruta">* Ingrese correo al cual desea que le enviemos esta cotizaci&oacute;n </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="300" align="left" valign="top"><select name="vendedor" class="textarea-tel111">
                                                        <option value="WEB">Seleccione...</option>
                                                        <?php
                                                        $vendedores = mysql_query("select * from seguridad where auth = 'cotizaciones' and tipo = 1 order by nombre", $dbh) or die(mysql_error());
                                                        while($rowVendedores = mysql_fetch_assoc($vendedores)){
                                                            echo '<option value = "'.$rowVendedores['user'].'">'.utf8_encode($rowVendedores['nombre']).'</option>';
                                                        }?>
                                                    </select></td>
                                                <td align="left" valign="top">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="top" class="texto-ruta">* Si fue
                                                    asistido por uno de nuestros vendedores, puede ingresarlo aqu&iacute;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="top">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="top">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" align="left" valign="top"><span
                                                            class="texto-ruta">(<span class="style1">*</span>) Campos son obligatorios </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="29" colspan="2" align="right" valign="bottom"><input
                                                            name="boton" type="image" id='boton' class='boton'
                                                            value="Submit" src="imagenes/BOTONCONTACTO.png"
                                                            align="bottom"/></td>
                                            </tr>
                                        </table>
                                    </form>
                                </td>
                            </tr>
                        </table>
                        <table width="440" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="30">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
