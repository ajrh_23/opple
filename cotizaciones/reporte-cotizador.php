<?php 
include "seguridad.php";
include "conect.php";
$rand = substr(md5(rand()),0,5);
$usuario = $_SESSION['nombre'];
if($usuario == "superadmin"){
  $sqlCotizaciones = mysql_query("select * from en_cotizaciongenerada order by id desc");
}else{
  $sqlCotizaciones = mysql_query("select * from en_cotizaciongenerada where vendedor = '".$usuario."' order by id desc");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>OPPLE CHILE - REPORTES - COTIZADOR</title>
<link href="css-rte-cotizador/estilos-cotizador.css" media="print" rel="stylesheet" type="text/css" />
<link href="css-rte-cotizador/estilos-cotizador.css" media="screen" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="../fb/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="../fb/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="../fb/jquery.fancybox-1.3.4.css" media="screen" />
<script type="text/javascript" src="js/jquery.uitablefilter.js"></script>
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-image: url(imagenes-reporte-cotiza/fondo-glob.jpg);
	background-repeat: repeat-x;
}
-->
</style>
<script type="text/javascript">
  function verDetalle(id, rd){
    $.fancybox({
        'width': 1200,
        'height': 620,
        'align': 'center',
        'autoSize': true,
        'transitionIn': 'fade',
        'transitionOut': 'fade',
        'centerOnScroll': true,
        'type': 'iframe',
        'href': "detalle.php?idc="+id,
        'onClosed': function(){ document.location="reporte-cotizador.php?rd="+rd; }
    });
  }
  function editarCotizacion(id, rd){
    $.fancybox({
        'width': 1200,
        'height': 620,
        'align': 'center',
        'autoSize': true,
        'transitionIn': 'fade',
        'transitionOut': 'fade',
        'centerOnScroll': true,
        'type': 'iframe',
        'href': "detalle_edit.php?idc="+id,
        'onClosed': function(){ document.location="reporte-cotizador.php?rd="+rd; }
    });
  }
  function verOriginal(id, rd){
    $.fancybox({
        'width': 1200,
        'height': 620,
        'align': 'center',
        'autoSize': true,
        'transitionIn': 'fade',
        'transitionOut': 'fade',
        'centerOnScroll': true,
        'type': 'iframe',
        'href': "detalle_respaldo.php?idc="+id,
        'onClosed': function(){ document.location="reporte-cotizador.php?rd="+rd; }
    });
  }
  $(document).ready(function(){
      $("#q").focus();
  });
  $(function() {
    theTable = $("#cotizaciones");
    $("#q").keyup(function() {
    $.uiTableFilter(theTable, this.value);
    });
  });
</script>
</head>
<body>
<table width="1087" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="1087" height="238" align="left" valign="top" background="imagenes-reporte-cotiza/up-rte.jpg">&nbsp;</td>
  </tr>
  <tr>
    <td width="1087" height="40" align="right" valign="top" class="texto-titulo2"><a href="index.php" target="_self"><img src="imagenes-reporte-cotiza/cerrar.jpg" width="99" height="24" border="0" class="no-decoro2" /></a><a href='javascript:window.print(); void 0;' target="_blank"><img src="imagenes-reporte-cotiza/imprimir.jpg" width="72" height="24" class="no-decoro" /></a></td>
  </tr>
  <tr>
    <td align="right" valign="top" class="texto-titulo2"><img src="imagenes-reporte-cotiza/icon.jpg" width="21" height="17" align="absmiddle" />DETALLE DE SUS COTIZACIONES RECIBIDAS</td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
    <td width="1087" align="center" valign="top"><table width="1020" align="center" cellpadding="0" cellspacing="2">
      <tr>
        <td align="right" colspan="8"><input type="text" id="q" name="q" value="" placeholder="Buscar..." class="textarea-contacto99" /></td>
      </tr>
    </table>
    </td>
  <tr>
    <td width="1087" align="center" valign="top"><table width="1020" align="center" cellpadding="0" cellspacing="2" id="cotizaciones">
      <thead>
      <tr>
        <td height="30" align="left" valign="middle" bgcolor="#2966A5" class="texto-titulos">N&deg; COTIZACI&Oacute;N </td>

        <td height="30" align="left" valign="middle" bgcolor="#2966A5" class="texto-titulos">RUT CLIENTE </td>

        <td height="30" align="left" valign="middle" bgcolor="#2966A5" class="texto-titulos">NOMBRE CLIENTE </td>

        <td height="30" align="left" valign="middle" bgcolor="#2966A5" class="texto-titulos">MONTO ($)</td>

        <td height="30" align="left" valign="middle" bgcolor="#2966A5" class="texto-titulos">FECHA EMISI&Oacute;N </td>

        <td height="30" align="center" valign="middle" bgcolor="#2966A5" class="texto-titulos">MODIFICADO</td>

        <td height="30" align="center" valign="middle" bgcolor="#2966A5" class="texto-titulos">ABRIR</td>

        <td height="30" align="center" valign="middle" bgcolor="#2966A5" class="texto-titulos">EDITAR</td>

        <td height="30" align="center" valign="middle" bgcolor="#2966A5" class="texto-titulos">VENDEDOR</td>
      </tr>
      </thead>
      <?php 
      if(mysql_num_rows($sqlCotizaciones)>0){
        while($row = mysql_fetch_assoc($sqlCotizaciones)){
          if($row['modificado'] == 0){
            $modificado = "No";
            $ver = '';
          }else{
            $modificado = "Si";
            $ver = ' - <a onclick="verOriginal('.$row['id'].',\''.$rand.'\')" style="cursor:pointer;">Ver original</a>';
          }
          $fecha = date_create($row['fecha_cotizacion']);?>
          <tr>
            <td height="30" align="left" valign="middle" bgcolor="#E1E5E9" class="texto-normal"><strong><?php echo $row['id']?></strong></td>
            <td height="30" align="left" valign="middle" bgcolor="#E1E5E9" class="texto-normal"><?php echo $row['rut']?></td>
            <td height="30" align="left" valign="middle" bgcolor="#E1E5E9" class="texto-normal"><?php echo $row['nombre']?></td>
            <td height="30" align="left" valign="middle" bgcolor="#E1E5E9" class="texto-normal">$<?php echo number_format($row['total'], 0, ',', '.')?></td>
            <td height="30" align="left" valign="middle" bgcolor="#E1E5E9" class="texto-normal"><?php echo date_format($fecha, 'd/m/Y')?></td>
            <td height="30" align="left" valign="middle" bgcolor="#E1E5E9" class="texto-normal"><?php echo $modificado.$ver?></td>
            <td height="30" align="center" valign="middle" bgcolor="#E1E5E9" class="no-decoro"><img src="imagenes-reporte-cotiza/abrir.jpg" width="30" height="29" border="0" style="cursor:pointer;" onclick="verDetalle('<?php echo $row['id']?>', '<?php echo $rand?>')" /></td>
            <td height="30" align="center" valign="middle" bgcolor="#E1E5E9" class="no-decoro"><img src="imagenes-reporte-cotiza/editar.jpg" width="30" height="29" border="0" style="cursor:pointer;" onclick="editarCotizacion('<?php echo $row['id']?>', '<?php echo $rand?>')" /></td>
            <td height="30" align="left" valign="middle" bgcolor="#E1E5E9" class="texto-normal"><?php echo $row['vendedor']?></td>
          </tr>
          <?php
        }
      }else{
        ?>
        <tr>
          <td height="30" align="left" valign="middle" bgcolor="#E1E5E9" class="texto-normal" colspan="9"><img src="imagenes-reporte-cotiza/none.jpg" width="26" height="19" align="left" /><span class="text-none">No existen cotizaciones ingresadas en el sistema</span></td>
        </tr>
        <?php
      }
      ?>
    </table>
    </td>
  </tr>
  <tr>
    <td width="1087" height="120" align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="138" align="center" valign="middle" background="imagenes-reporte-cotiza/fondo-rte.jpg"><table width="1084" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="1084" height="137" align="left" valign="middle" background="imagenes-reporte-cotiza/dawn-rte.jpg" class="TEXTO-ABAJO">&nbsp;<strong>SOPORTE:</strong><br />
          &nbsp;FIJO: (+56 23) 269 71 79<br />
          &nbsp;M&Oacute;VIL: (+56 9) 798 52 428 - (+56 9) 959 17 476<br />
          &nbsp;CORREO: <a href="mailto:info@3amweb.cl" target="_self">INFO@3AMWEB.CL</a></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
</body>
</html>