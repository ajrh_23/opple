<?php include "conect.php"; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php include "head_interna.php"; ?>
<?php
$titulo = 'Cont&aacute;ctenos';
$link = 'contacto.php';
$query_foot = mysql_query("select * from footer", $dbh) or die(mysql_error());
$row_foot = mysql_fetch_assoc($query_foot);
$footer = $row_foot['contenido'];

function getRoute($path, $nombre)
{
    global $dbh;
    $route = array();
    $route[0] = utf8_encode('<li><img src="imagenes/homeicon.png" width="16" height="12" /><a href="index.php" target="_self">Home</a>&nbsp;&nbsp;></li>');
    $busca_nivel_2 = mysql_query("select * from submenu where link = '" . $path . "'", $dbh) or die(mysql_error());
    if (mysql_num_rows($busca_nivel_2) > 0) {
        $row_nivel_2 = mysql_fetch_assoc($busca_nivel_2);
        $id_menu_principal = $row_nivel_2['id_menu'];
        $busca_nivel_1 = mysql_query("select boton from menu where id = " . $id_menu_principal, $dbh) or die(mysql_error());
        $row_nivel_1 = mysql_fetch_assoc($busca_nivel_1);
        $nivel_1 = $row_nivel_1['boton'];
        $route [1] = utf8_encode('<li>' . $nivel_1 . '&nbsp;&nbsp;></li>');
        $route [2] = utf8_encode('<li>' . $nombre . '</li>');
    } else {
        $route [1] = utf8_encode('<li>' . $nombre . '</li>');
    }
    return $route;
}

function getMenuLateral($path, $nombre)
{
    global $dbh;
    $busca_asociados = mysql_query("select b.boton, a.nombre, a.link from submenu a 
    inner join menu b on a.id_menu = b.id
    where a.id_menu in (
	  select c.id_menu from submenu c where c.link = '" . $path . "'
    )
    order by a.id", $dbh) or die(mysql_error());
    $titulo = '';
    $contenido = '';
    $i = 0;
    while ($row_asociados = mysql_fetch_assoc($busca_asociados)) {
        if ($i == 0) {
            $titulo = '<li class="accordion-COLOR1">' . strtoupper($row_asociados['boton']) . '</li>';
        }
        if ($row_asociados['link'] == $path) {
            $contenido .= '<li><span id="activado"><a href="' . $row_asociados['link'] . '">' . $row_asociados['nombre'] . '</a></span></li>';
        } else {
            $contenido .= '<li><a href="' . $row_asociados['link'] . '">' . $row_asociados['nombre'] . '</a></li>';
        }
        $i++;
    }
    $menu_lateral = $titulo;
    $menu_lateral .= '<ul>';
    $menu_lateral .= $contenido;
    $menu_lateral .= '</ul>';

    return $menu_lateral;
}

?>
<body onload="MM_preloadImages('imagenes/homebuttom2.png','imagenes/Facelog2.png','imagenes/youtubelog2.png')">
<div id="contenedor">
    <div id="arriba"></div>
    <div id="arriba2">
        <?php include "top.php"; ?>
    </div>
    <div id="menu-principal">
        <?php include "menu.php"; ?>
    </div>
    <div id="espacio"></div>
    <div id="espacio2">
        <table width="1163" height="185" border="0" cellpadding="0" cellspacing="0">
            <tr>
                <td width="507" align="left" valign="top" bgcolor="1952AE" class="texto-titulo-internas">CONTACTO</td>
                <td width="12" align="left" valign="top">&nbsp;</td>
                <td width="644" align="left" valign="top"><img src="imagenes/contact.png" width="644" height="185"/>
                </td>
            </tr>
        </table>
    </div>
    <div id="espacio3"></div>
    <div id="medio3">
        <div id="menuizq">
            <ul id="accordion">
                <?php echo utf8_encode(getMenuLateral($link, $titulo)); ?>
            </ul>
        </div>
        <div id="medio4"></div>
        <div id="medio44">
            <table width="870" border="0" align="right" cellpadding="0" cellspacing="0">
                <tr align="right" valign="top">
                    <td>
                        <ul id="mi_lista">
                            <?php
                            $path = getRoute($link, $titulo);
                            foreach ($path as $route) {
                                echo $route;
                            }
                            ?>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
        <div id="medio5"></div>
        <div id="medio55"></div>
        <div id="medio56">
            <table width="880" border="0" align="left" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="880" align="left" valign="top" class="texto-principal-titulo">Cont&aacute;ctenos</td>
                </tr>
                <tr>
                    <td width="880" align="left" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td width="880" align="left" valign="top"><div align="justify">
                            <table width="880" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="880" align="left" valign="top" class="texto-principal1"><p><img src="imagenes/flecha.png" width="30" height="30" align="absmiddle" /> Gracias, hemos recibido su mensaje satisfactoriamente, nos pondremos en contacto con usted.</p>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <p align="center" class="texto-ruta"><a href="contacto.php" target="_self">Volver</a></p></td>
                                </tr>
                            </table>
                            <p class="texto-principal1"><span class="texto-principal1"><strong><br />
            </strong></span></p>
                        </div></td>
                </tr>
            </table>
        </div>
        <div id="medio9"></div>
    </div>
    <?php echo utf8_encode($footer); ?>
</div>
</body>
<script>
    $("#accordion > li").click(function () {

        if (false == $(this).next().is(':visible')) {
            $('#accordion > ul').slideUp(300);
        }
        $(this).next().slideToggle(300);
    });

    $('#accordion > ul:eq(0)').show();
</script>
</html>